const nodemailer = require('nodemailer')
const mongoose = require('mongoose')
const Email = require('../models/email')
let auth = {
    type: 'oauth2',
    user:process.env.EMAIL,
    clientId: process.env.CLIENTID,
    clientSecret: process.env.CLIENTSEC,
    refreshToken: process.env.REFTOK
}

exports.sendmail = (req,res,next) => {
    try{
        let email = new Email({
            _id: new mongoose.Types.ObjectId,
            name: req.body.name,
            email: req.body.email,
            subject: req.body.subject,
            message: req.body.message
        })
        email.save()
        let mailOptions = {
            from: req.body.name,
            to: process.env.EMAIL,
            subject: `${req.body.subject} : ${req.body.name}`,
            text: req.body.message
        }
        let transporter = nodemailer.createTransport({
            service:'gmail',
            auth:auth
        })

        transporter.sendMail(mailOptions,(err,res) => {
            if(err){
               res.status('500').json({error:err}) 
            }
            else{
                res.status('200').json({message:'Email saved and sent'})
            }
        })

    }
    catch(err){
        res.status(500).json({error:err})
        console.log(err)
    }

}
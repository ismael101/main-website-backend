const mongoose = require('mongoose')
const express = require('express')
const morgan = require('morgan')
const email = require('./routes/email')
const app = express()

mongoose.connect(`mongodb://${process.env.USER}:${process.env.PASS}@ds055564.mlab.com:55564/${process.env.DB}`,{useNewUrlParser:true, useCreateIndex:true, useUnifiedTopology:true})
        .then(() => {
            console.log('connected to mlab')
        })
        .catch(err => {
            console.log(`error occured ${err}`)
        })

app.use(express.json())

app.use(morgan('dev'))
app.use('/email',email)

app.use((req,res,next) => {
	res.status(404).send("Not Found")
})
app.use((err,req,res,next) => {
	console.error(err.stack)
	res.status(500).send("Server Error")
})
app.listen(process.env.PORT,() => {  
    console.log(`Server running on port ${process.env.PORT}`)
})

